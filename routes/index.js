var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Binar Chapter 3 | Yusuf Ginanjar' });
});

module.exports = router;
