var express = require('express');
var router = express.Router();
var db = require('../database/user.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Chapter 5 | Login', errorMessage : '' });
});

router.post('/', function(req, res, next) {
    var loginSuccess = false;  // init value for loginSuccess is false
    for (let i = 0; i < db.length; i++) {
        if((req.body.username === db[i].username) && (req.body.password === db[i].password)){
            loginSuccess = true; // Set loginSuccess = true if user input correct username & password
        }
    }

    if(loginSuccess === true){
        res.status(200);

        /* OPTION!! : uncomment one of the options */
        /* 1. if you want to serve user static data */
            res.status(200).json(db);
        /* 2. if you want to redirect to a specific url */
        //  res.redirect('/rock-paper-scissors');


    } else {
        res.status(401);
        res.render('login', { title: 'Login', errorMessage : 'Wrong username or password' });
    }
});



module.exports = router;
